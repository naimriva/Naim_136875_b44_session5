<?php
//boolean type example start here
$decision=true;
//if(expression==true){do this}
if($decision)
    echo "the decision is true <br>";

$decision=false;

if($decision)
    echo "the decision is false";
//boolean type example ended here

//integer type example start here
$value=100;
echo $value."<br>";
//integer type example ended here


//float type example start here
$value=56.8655;
echo $value."<br>";

//float type example ended here

//string type example start here
$v=100;
$var1='this is single quoted string $v <br>';
$var2="this is duble quoted string $v <br>";
echo $var1.$var2;

//sting type example ended here

//heredoc type example start here

$heredocvar1=<<<BITM

this is here doc example line1 $value <br>
this is here doc example line2 <br>
this is here doc example line3 <br>
this is here doc example line4 <br>
BITM;
//heredoc type example ended here


//nowdoc type example start here
$nowdocvar1=<<<'BITM'

this is now doc example line1 <br>
this is now doc example line2 $value <br>
this is now doc example line3 <br>
this is now doc example line4 <br>
BITM;

echo $heredocvar1."<br><br>" .$nowdocvar1;
//nowdoc type example ended here



//array type example start here

$arr=array(1,2,3,4,5,6,7,8,9);
print_r($arr);

echo "<br>";
$arr=array("BMW","TOYOTA","NISSAN","FERARi","MARUTI") ;
print_r($arr);
echo "<br>";
echo "<br>";

//people age define
$age=array("Riva"=>22,"sammu"=>22,"kasfi"=>25);
//find only riva's age
echo "the age of riva is : ". $age["Riva"];
echo "<br>";
//print_r($age);

//array type example ended here
?>

